/*
 * @(#) SimpleTextViewerDialog.java
 *
 * Created on 24.01.2010 by Daniel Becker
 *
 *-----------------------------------------------------------------------
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *----------------------------------------------------------------------
 */
package de.quippy.javamod.main.gui;

import de.quippy.javamod.system.Helpers;

import javax.swing.*;
import java.awt.*;

/**
 * @author Daniel Becker
 * @since 24.01.2010
 */
public class SimpleTextViewerDialog extends JDialog {
    private static final long serialVersionUID = -5666092255473846648L;

    private static final String DEFAULT_CODING = "ISO-8859-1";

    private javax.swing.JButton closeButton;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JTextArea textArea;
    private String content;
    private String coding;

    /**
     * Constructor for SimpleTextViewerDialog
     *
     * @param owner
     * @throws HeadlessException
     */
    public SimpleTextViewerDialog(JFrame owner, boolean modal) {
        super(owner, modal);
        content = null;
        coding = DEFAULT_CODING;
        initialize();
    }

    public SimpleTextViewerDialog(JFrame owner, boolean modal, String resourcePath, String coding) {
        this(owner, modal);
        this.coding = coding;
        setDisplayTextFromResource(resourcePath);
    }

    private void initialize() {
        setTitle("File Viewer");
        setName("SimpleTextFileViewer");
        setSize(new Dimension(640, 480));
        setPreferredSize(getSize());
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                doClose();
            }
        });

        setLayout(new java.awt.GridBagLayout());
        Container panel = getContentPane();

        panel.add(getScrollPane(), Helpers.getGridBagConstraint(0, 0, 1, 0, java.awt.GridBagConstraints.BOTH, java.awt.GridBagConstraints.WEST, 1.0, 1.0));
        panel.add(getCloseButton(), Helpers.getGridBagConstraint(0, 1, 1, 0, java.awt.GridBagConstraints.NONE, java.awt.GridBagConstraints.CENTER, 0.0, 0.0));

        if (content != null) {
            fillTextArea();
        }

        pack();
    }

    private JScrollPane getScrollPane() {
        if (scrollPane == null) {
            scrollPane = new JScrollPane();
            scrollPane.setName("scrollPane_TextField");
            scrollPane.setViewportView(getTextArea());
        }
        return scrollPane;
    }

    private JTextArea getTextArea() {
        if (textArea == null) {
            textArea = new JTextArea();
            textArea.setName("modInfo_Instruments");
            textArea.setEditable(false);
            textArea.setFont(Helpers.getTextAreaFont());
        }
        return textArea;
    }

    private JButton getCloseButton() {
        if (closeButton == null) {
            closeButton = new JButton();
            closeButton.setMnemonic('C');
            closeButton.setText("Close");
            closeButton.setToolTipText("Close");
            closeButton.setFont(Helpers.getDialogFont());
            closeButton.addActionListener(evt -> doClose());
        }
        return closeButton;
    }

    public void doClose() {
        setVisible(false);
        dispose();
        //if we are alone in the world, exit the vm
        if (getParent() == null) {
            System.exit(0); // this should not be needed!
        }
    }

    private void fillTextArea() {
        if (this.content != null) {
            getTextArea().setText(content);
            getTextArea().select(0, 0);
        }
    }

    public void setDisplayTextFromResource(String path) {
        this.content = Helpers.getResourceAsString(path);
        fillTextArea();
    }
}
