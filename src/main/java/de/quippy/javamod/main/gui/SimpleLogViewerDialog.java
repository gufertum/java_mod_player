/*
 * @(#) SimpleTextViewerDialog.java
 *
 * Created on 24.01.2010 by Daniel Becker
 *
 *-----------------------------------------------------------------------
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *----------------------------------------------------------------------
 */
package de.quippy.javamod.main.gui;

import de.quippy.javamod.main.gui.components.JTextAreaAppender;
import de.quippy.javamod.system.Helpers;

import javax.swing.*;
import java.awt.*;

/**
 * @author Daniel Becker
 * @since 24.01.2010
 */
public class SimpleLogViewerDialog extends JDialog {
    private static final long serialVersionUID = -5654092255473846648L;

    private static final String DEFAULT_CODING = "ISO-8859-1";

    private JButton closeButton;
    private JScrollPane scrollPane;
    private JTextArea textArea;

    /**
     * Constructor for SimpleTextViewerDialog
     *
     * @param owner
     * @throws HeadlessException
     */
    public SimpleLogViewerDialog(JFrame owner, boolean modal) {
        super(owner, modal);
        initialize();
    }

    private void initialize() {
        setTitle("Log Viewer");
        setName("SimpleLogFileViewer");
        setSize(new Dimension(640, 480));
        setPreferredSize(getSize());
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                doClose();
            }
        });

        setLayout(new GridBagLayout());
        Container panel = getContentPane();

        panel.add(getScrollPane(), Helpers.getGridBagConstraint(0, 0, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.WEST, 1.0, 1.0));
        panel.add(getCloseButton(), Helpers.getGridBagConstraint(0, 1, 1, 0, GridBagConstraints.NONE, GridBagConstraints.CENTER, 0.0, 0.0));

        pack();
    }

    private JScrollPane getScrollPane() {
        if (scrollPane == null) {
            scrollPane = new JScrollPane();
            scrollPane.setName("scrollPane_TextField");
            scrollPane.setViewportView(getTextArea());
            scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        }
        return scrollPane;
    }

    private JTextArea getTextArea() {
        if (textArea == null) {
            textArea = new JTextArea(50, 80);
            textArea.setName("modInfo_Instruments");
            textArea.setEditable(false);
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(false);
            textArea.setFont(Helpers.getTextAreaFont());
            JTextAreaAppender.addTextArea(textArea); //initialize the logger output
        }
        return textArea;
    }

    private JButton getCloseButton() {
        if (closeButton == null) {
            closeButton = new JButton();
            closeButton.setMnemonic('C');
            closeButton.setText("Close");
            closeButton.setToolTipText("Close");
            closeButton.setFont(Helpers.getDialogFont());
            closeButton.addActionListener(evt -> doClose());
        }
        return closeButton;
    }

    public void doClose() {
        setVisible(false);
        dispose();
        //if we are alone in the world, exit the vm
        if (getParent() == null) {
            System.exit(0); // this should not be needed!
        }
    }
}
