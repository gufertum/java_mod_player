/*
 * Created on Jun 28, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package de.quippy.jflac;

import de.quippy.jflac.frame.Frame;
import de.quippy.jflac.metadata.Metadata;

import java.util.HashSet;
import java.util.Iterator;


/**
 * Class to handle frame listeners.
 *
 * @author kc7bfi
 */
class FrameListeners implements FrameListener {
    private final HashSet<FrameListener> frameListeners = new HashSet<>();

    /**
     * Add a frame listener.
     *
     * @param listener The frame listener to add
     */
    public void addFrameListener(FrameListener listener) {
        synchronized (frameListeners) {
            frameListeners.add(listener);
        }
    }

    /**
     * Remove a frame listener.
     *
     * @param listener The frame listener to remove
     */
    public void removeFrameListener(FrameListener listener) {
        synchronized (frameListeners) {
            frameListeners.remove(listener);
        }
    }

    /**
     * Process metadata records.
     *
     * @param metadata the metadata block
     */
    @Override
    public void processMetadata(Metadata metadata) {
        synchronized (frameListeners) {
            for (FrameListener listener : frameListeners) {
                listener.processMetadata(metadata);
            }
        }
    }

    /**
     * Process data frames.
     *
     * @param frame the data frame
     * @see de.quippy.jflac.FrameListener#processFrame(de.quippy.jflac.frame.Frame)
     */
    @Override
    public void processFrame(Frame frame) {
        synchronized (frameListeners) {
            for (FrameListener listener : frameListeners) {
                listener.processFrame(frame);
            }
        }
    }

    /**
     * Called for each frame error detected.
     *
     * @param msg The error message
     * @see de.quippy.jflac.FrameListener#processError(java.lang.String)
     */
    @Override
    public void processError(String msg) {
        synchronized (frameListeners) {
            for (FrameListener listener : frameListeners) {
                listener.processError(msg);
            }
        }
    }

}
